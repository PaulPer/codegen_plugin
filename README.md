### README ###

A plugin to automatically generate headers/interfaces from USE class diagram

Maybe implemetation code can be generated from ocl, in a later update

### Generic plugin creation tips ###
 The plugin must contain the folowing files :
     META-INF/Manifest.mf
       -> This is a normal manifest file, but it must specify elements 'Plugin-Name', 'Plugin-Version', 'Plugin-Publisher', and 'Main-Class'
     useplugin.xml
       -> This is a plugin definition file, telling use what entries to add to its menus, and which classes to call on click

 Some implementation classes must be then created.
      The main class must extend Plugin and implement IPlugin.
      The classes called on menu item click must implement IPluginActionDelegate


### Where to start coding ###
When a button is clicked in the menu, one of org/tzi/use/gui/plugins/codegen/Gen<Language>.java:performAction(args) is called
These methods must be implemented. Anything not language specific should go in the mother class org/tzi/use/gui/plugins/codegen/GenHeaders.java



### Please contribute ###
Trello : https://trello.com/b/IySEWzcY/usecodegenplugin
Gitlab : https://gitlab.com/USE-OCL/codegen_plugin
