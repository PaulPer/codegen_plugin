/*
**
*/

package org.tzi.use.codegen;

import org.tzi.use.uml.mm.MClass;
import org.tzi.use.uml.mm.MAssociation;
import org.tzi.use.uml.mm.MAttribute;
import org.tzi.use.uml.mm.MOperation;
import org.tzi.use.uml.mm.MModel;
import org.tzi.use.uml.ocl.type.Type;
import org.tzi.use.uml.ocl.expr.VarDecl;
import org.tzi.use.uml.ocl.expr.VarDeclList;


public class CppWriter extends CodeWriter {

    private Scope currentScope = Scope.DEFAULT;
    private String getSet = "";
    private String constructor = "";
    private String publicStatements = "";
    private String privateStatements = "";
    private String namespace = "";

    public CppWriter() {
	super();
	fillTypeNames();
    }

    public void beginFile(MModel fModel, MClass fClass) {
	//	System.out.println("Beginning CPP file " + fClass.name());
	this.filename = fClass.name() + ".hpp";
	this.namespace = fModel.name();

	this.content = "/*\n** Header file generated with USE codegen plugin\n*/\n\n";
	this.content += "\n #ifndef " + fClass.name().toUpperCase() + "_HPP_";
	this.content += "\n #define " + fClass.name().toUpperCase() + "_HPP_\n\n";

	this.content += "using namespace " + fModel.name() + ";\n\n";

	this.getSet = "";
	this.constructor = "";
	this.publicStatements = "";
	this.privateStatements = "";


    }
    public void endFile(MClass fClass) {
	this.content += "\n #endif !/* " + fClass.name().toUpperCase() + "_HPP_ */\n";
    }

    public void beginClass(MClass fClass) {
	String inherit = "";
	if (!fClass.parents().isEmpty()) {
	    for (MClass parent : fClass.parents()) {
		inherit += ", public " + parent.name();
	    }
	    inherit = ':' + inherit.substring(1);
	}

	this.content += "class " + fClass.name() + inherit + " {\n";

	this.publicStatements += "\t// Constructors\n";
	this.publicStatements += "\t"+fClass.name()+"();\n";
	this.publicStatements += "\t~"+fClass.name()+"();\n\n\n";
    }
    public void endClass(MClass fClass) {
	setScope(Scope.PRIVATE);
	this.content += this.privateStatements;
	setScope(Scope.PUBLIC);
	this.content += this.publicStatements + "\n\n\t// Getters and Setters\n" + this.getSet;
	this.content += "\n\n};\n";
    }

    public void addOperation(MOperation fOperation) {
	// add (public) entry like [type] [operation_name] ( [args_list] ) ; \n

	publicStatements += "\t" + getType(fOperation.resultType()) + "\t" + fOperation.name() + "(";

	for (int i=0; i<fOperation.paramList().size(); i++) {
	    if (i > 0)
		publicStatements += ", ";

	    publicStatements += getType(fOperation.paramList().varDecl(i).type());
	    publicStatements += " ";
	    publicStatements += fOperation.paramList().varDecl(i).name();

	}

	publicStatements += ");\n";

    }
    public void addAssociation(MAssociation fAssociation) {

	int	multiplicity = 0;	// Get actual multiplicity
	String	name = "";		// Get actual name of associated object
	String	type = "";		// Get acual type of associated object

	//       	privateStatements += "\t" + type + "\t*"+name+";\n";
	//	constructor += "\t\t"+name+" = new "+type+"["+multiplicity+"];";

	// add (private) entry like [associated_object_type] [role_name] ; \n

	// get type, multiplicity and role_name for other end
	// if composition, add Constructor/Destructor to delete child if parent dies

	// content += "\t" + getType(fAssociation.type()) + "\t" + fAssociation.name() + ";\n";


    }
    public void addAttribute(MAttribute fAttribute) {
	// add (public) entry like [type] [attribute_name] ; \n
	// Should change to private attribute + getter/setter
	String name = fAttribute.name();
	String type = getType(fAttribute.type());
	privateStatements += "\t" + type + "\t" + name + ";\n";

	String capitalName = Character.toUpperCase(name.charAt(0)) + name.substring(1);
	getSet += "\t" + type + "\tget" + capitalName + "() { return "+name+" ; }\n";
	getSet += "\tvoid\tset" + capitalName + "("+type+" value) { "+name+" = value ; }\n\n";
    }

    private void setScope(Scope scope) {
	if (currentScope != scope) {
	    switch (scope) {
	    case PUBLIC:
		content += "\npublic:\n";
		break;
	    case PRIVATE:
		content += "\nprivate:\n";
		break;
	    case PROTECTED:
		content += "\nprotected:\n";
		break;


	    }
	    currentScope = scope;
	}

    }


    protected void fillTypeNames() {
	typeNames.put("int", "int");
	typeNames.put("char", "char");
	typeNames.put("string", "std::string");
	typeNames.put("real", "double");
	typeNames.put("boolean", "boolean");
	typeNames.put("void", "void");

    }
}
