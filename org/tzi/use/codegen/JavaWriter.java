/*
**
*/

package org.tzi.use.codegen;

import org.tzi.use.uml.mm.MClass;
import org.tzi.use.uml.mm.MAssociation;
import org.tzi.use.uml.mm.MAttribute;
import org.tzi.use.uml.mm.MOperation;
import org.tzi.use.uml.mm.MModel;



public class JavaWriter extends CodeWriter {

    public void beginFile(MModel fModel, MClass fClass) {
	System.out.println("Beginning JAVA file " + fClass.name());
    }
    public void endFile(MClass fClass) {

    }

    public void beginClass(MClass fClass) {

    }
    public void endClass(MClass fClass) {

    }

    public void addOperation(MOperation fOperation) {

    }
    public void addAssociation(MAssociation fAssociation) {

    }
    public void addAttribute(MAttribute fAttribute) {

    }

}
