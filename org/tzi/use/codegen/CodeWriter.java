/*
**
*/

package org.tzi.use.codegen;

import org.tzi.use.uml.mm.MClass;
import org.tzi.use.uml.mm.MAssociation;
import org.tzi.use.uml.mm.MAttribute;
import org.tzi.use.uml.mm.MOperation;
import org.tzi.use.uml.mm.MModel;
import org.tzi.use.uml.ocl.type.Type;
import java.util.HashMap;

import java.io.*;

public abstract class CodeWriter {

    protected String filename;
    protected String content;
    protected HashMap<String, String> typeNames;

    public CodeWriter() {
	typeNames = new HashMap<String, String>();
	fillTypeNames();
    }

    public void newFile(String filename) {
	this.filename = filename;
	this.content = "";
    }

    public abstract void beginFile(MModel fModel, MClass fClass);
    public abstract void endFile(MClass fClass);

    public abstract void beginClass(MClass fClass);
    public abstract void endClass(MClass fClass);

    public abstract void addOperation(MOperation fOperation);
    public abstract void addAssociation(MAssociation fAssociation);
    public abstract void addAttribute(MAttribute fAttribute);

    public void write() {

	try {
	    FileOutputStream out = new FileOutputStream(filename);
	    out.write(content.getBytes());
	    out.close();
	} catch (IOException e) {
	    System.out.println("File write failed - " + filename);
	}
    }

    protected String getType(Type type) {
	try {
	    if (type.isTypeOfInteger())
		return typeNames.get("int");
	    if (type.isTypeOfBoolean())
		return typeNames.get("boolean");
	    if (type.isTypeOfReal())
		return typeNames.get("real");
	    if (type.isTypeOfString())
		return typeNames.get("string");
	    return typeNames.get("void");
	} catch (Exception e) {
	    System.out.println("NO SUCH TYPE FOUND : " + type.shortName() + " - using 'void'");
	    return "void";
	}
    }


    protected void fillTypeNames() {
	// example. Using C++ types here
	typeNames.put("int", "int");
	typeNames.put("char", "char");
	typeNames.put("string", "std::string");
	typeNames.put("real", "double");
	typeNames.put("boolean", "boolean");
	typeNames.put("void", "void");

    }

    protected enum Scope {PRIVATE, PUBLIC, PROTECTED, DEFAULT}
}
