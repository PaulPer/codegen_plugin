#!/bin/sh
##
## build.sh for build in /home/vincent/USE/codegen_plugin
## 
## Made by 
## <vincent.davoust@gmail.com>
## 
## Started on  Mon May  7 14:03:53 2018 
## Last update Tue May  8 14:40:52 2018 
##



cd org/tzi/use/codegen
javac -cp ../../../../../use-5.0.1/lib/use.jar *.java
cd ../../../../
cd org/tzi/use/gui/plugins/codegen
javac -cp ../../../../../../../use-5.0.1/lib/use.jar:../../../../../../ *.java
cd ../../../../../../
jar cfm ../use-5.0.1/lib/plugins/codegen_plugin.jar Manifest.txt resources org useplugin.xml
cp ../use-5.0.1/lib/plugins/codegen_plugin.jar ./
